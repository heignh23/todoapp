const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const TodoModel = require('./Models/Todo')

const app = express()
app.use(cors()) // Enable Cross-Origin Resource Sharing (CORS)
app.use(express.json())

// Conect to the MongoDB database
mongoose.connect('mongodb://127.0.0.1:27017/test')

// Handle GET requests to retrieve tasks from the database
app.get('/get', (req, res) => {
    TodoModel.find()
    .then(result => res.json(result))
    .catch(err => res.json(err))
})

// Handle PUT requests to mark tasks as done in the database
app.put('/update/:id', (req, res) => {
    const {id} = req.params;
    TodoModel.findByIdAndUpdate({_id: id}, {done: true})
    .then(result => res.json(result))
    .catch(err => res.json(err))
})

// Handle DELETE requests to delete a task from the database
app.delete('/delete/:id', (req, res) => {
    const {id} = req.params;
    TodoModel.findByIdAndDelete({_id: id})
    .then(result => res.json(result))
    .catch(err => res.json(err))
})

// Handle POST requests to add new tasks to the database
app.post('/add', (req, res) => {
    const task = req.body.task; // this is our task variable
    TodoModel.create({
        task: task
    }).then(result => res.json(result))
    .catch(err => res.json(err))
})

app.listen(3001, () => {
    console.log("Server running")
})