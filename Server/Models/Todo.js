const mongoose = require('mongoose')

// This is the template for our tasks, as a component
const TodoSchema = new mongoose.Schema({
    // We add two fields for each task. The name (string) and if it is completed (boolean). 
    // The default value is false.

    task: String, // Name of the task
    done: {       // Completed?
        type: Boolean, 
        default: false 
    }
})

const TodoModel = mongoose.model("todos", TodoSchema)
module.exports = TodoModel