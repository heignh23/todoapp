import React, { useState } from 'react'
import axios from 'axios'

// This is the Create component. 
// It includes a text input for entering a new task
// and an 'add' button to add the task to the list.
function Create() {
  const [task, setTask] = useState()

  // This function sends a POST request to the server to add a new task.
  // Then it reloads the page automatically to display the recently added task.
  const handleAdd = () => {
    axios.post('http://localhost:3001/add', { task: task })
      .then(result => {
        location.reload() // This will automatically update the list when adding a new task
      })
      .catch(err => console.log(err))
  }
  
  return (
    <div className='create_form'>
      <input type="text" placeholder='Enter new task' onChange={(e) => setTask(e.target.value)} />
      <button type='button' onClick={handleAdd}>Add</button>
    </div>
  )
}

export default Create