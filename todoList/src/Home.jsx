import React, { useEffect, useState } from 'react'
import Create from './Create'
import axios from 'axios'
import { BsFillCheckCircleFill, BsFillTrashFill, BsCircleFill } from 'react-icons/bs';

// This is the Home component.
// It displays the To-Do list and allows users to add, edit or delete tasks.
function Home() {
    const [todos, setTodos] = useState([])

    // Fetches of the list of tasks from the server
    useEffect(() => {
        axios.get('http://localhost:3001/get')
        .then(result => setTodos(result.data))
        .catch(err => console.log(err))
    }, [])

    // PUT request to the server to mark as completed and then reloads the list.
    const handleEdit = (id) => {
        axios.put('http://localhost:3001/update/'+id)
        .then(result => {
            location.reload() // This will automatically update the list when adding a new task
        })
        .catch(err => console.log(err))
    }

    // DELETE request to the server to remove a task and then reloads the list.
    const handleDelete = (id) => {
        axios.delete('http://localhost:3001/delete/'+id)
        .then(result => {
            location.reload() // This will automatically update the list when deleting a task
        })
        .catch(err => console.log(err))
    }

  return (
    <div className='home'>
        <h2>To-Do List</h2>
        <Create />
        <br />
        {
            // If the todo list is empty, we show a message
            todos.length === 0 
            ?
            <div><h2>Your list is empty!</h2></div>
            :
            todos.map(todo => (
                <div className='task'>
                    <div className='checkbox' onClick={() => handleEdit(todo._id)}>
                        {todo.done ? 
                            <BsFillCheckCircleFill className='icon'></BsFillCheckCircleFill>
                        : <BsCircleFill className='icon'/>
                        }
                        <p className={todo.done ? "line_through" : ""}>{todo.task}</p>
                    </div>
                        <BsFillTrashFill className='tresh'
                            onClick={() => handleDelete(todo._id)}/>
                </div>
            ))
        }
    </div>
  )
}

export default Home