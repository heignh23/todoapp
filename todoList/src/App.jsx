import { useState } from 'react'
import './App.css'
import Home from './Home'

// This is the main component of the application.
// It renders the Home component, which is the main part of the app.

function App() {
  return (
    <div>
      <Home></Home>
    </div>
  )
}

export default App